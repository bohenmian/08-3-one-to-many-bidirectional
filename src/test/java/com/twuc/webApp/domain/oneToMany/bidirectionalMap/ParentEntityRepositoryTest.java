package com.twuc.webApp.domain.oneToMany.bidirectionalMap;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.assertj.core.api.Assertions.fail;
import static org.junit.jupiter.api.Assertions.*;

class ParentEntityRepositoryTest extends JpaTestBase {
    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Test
    void should_save_relationship_from_child_site() {
        // TODO
        //
        // 请书写测试实现如下的功能：
        //
        // Given parent 和 child 对象。并且它们均已经持久化。
        // When 我们在 child 对象上为其添加 parent 对象引用，并持久化后。
        // Then 当我们重新查询 parent 对象的时候，可以发现在 parent 对象的 children 列表中存在 child 对象。
        //
        // <--start-

        ClosureValue<Long> child = new ClosureValue<>();
        ClosureValue<Long> parent = new ClosureValue<>();
        flushAndClear(em -> {
            ParentEntity parentEntity = new ParentEntity("parent");
            ParentEntity saveParent = parentEntityRepository.save(parentEntity);
            parent.setValue(saveParent.getId());

            ChildEntity childEntity = new ChildEntity("child");
            ChildEntity saveChild = childEntityRepository.save(childEntity);
            child.setValue(saveChild.getId());
        });

        flushAndClear(em -> {
            ParentEntity parentEntity = parentEntityRepository.findById(parent.getValue()).orElseThrow(RuntimeException::new);
            ChildEntity childEntity = childEntityRepository.findById(child.getValue()).orElseThrow(RuntimeException::new);
            parentEntity.addChild(childEntity);
        });

        run(em -> {
            ParentEntity parentEntity = parentEntityRepository.findById(parent.getValue()).orElseThrow(RuntimeException::new);
            assertEquals(1, parentEntity.getChildEntities().size());
        });
        // --end->
    }

    @Test
    void should_save_and_get_parent_and_child_relationship_at_once() {
        // TODO
        //
        // 请书写测试实现如下的功能：
        //
        // Given parent 和 child 对象。并且它们均没有被持久化。
        // When 我们在 parent 对象上为其添加 child 对象引用，并将 parent 对象持久化。
        // Then 当我们重新查询 child 对象的时候，可以发现 child 对象中存在对 parent 对象的引用。
        //
        // <--start-
        ClosureValue<Long> parent = new ClosureValue<>();
        ChildEntity childEntity = new ChildEntity("child");
        ParentEntity parentEntity = new ParentEntity("parent");
        flushAndClear(em -> {
            parentEntity.addChild(childEntity);
            ParentEntity saveEntity = parentEntityRepository.save(parentEntity);
            parent.setValue(saveEntity.getId());
        });

        run(em -> {
            ChildEntity savedChild = childEntityRepository.findById(2L).orElseThrow(RuntimeException::new);
            assertEquals("parent", savedChild.getParentEntity().getName());

        });
        // --end->
    }

    @Test
    void should_remove_child_item() {
        // TODO
        //
        // 请书写如下的测试。
        //
        // Given parent 和 child 对象，并且其已经相互引用并已经持久化。
        // When 当我们从 parent 的 children 列表中移除 child 对象并持久化时。
        // Then 当我们再次查询 parent 的时候，发现 parent 的 children 列表中为空。同时 child 记录本身也
        //   不复存在了。
        //
        // <--start-
        flushAndClear(em -> {
            ChildEntity childEntity = new ChildEntity("child");
            ParentEntity parentEntity = new ParentEntity("parent");
            parentEntity.addChild(childEntity);
            parentEntityRepository.save(parentEntity);
        });

        flushAndClear(em -> {
            ParentEntity savedParent = parentEntityRepository.findById(1L).orElseThrow(RuntimeException::new);
            savedParent.getChildEntities().remove(new ChildEntity("child"));
        });

        run(em -> {
            assertEquals(0, parentEntityRepository.findById(1L).orElseThrow(RuntimeException::new).getChildEntities().size());
        });

        // --end->
    }
}
